const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

const { Student } = require('../models/student');

router.get('/', (req, res) => {
    Student.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Student Data :' + JSON.stringify(err, undefined, 2)); }
    });
});


router.post('/', (req, res) => {
    let stu = new Student({
        name: req.body.name,
        register_number: req.body.register_number,
        physics: req.body.physics,
        chemistry: req.body.chemistry,
        maths: req.body.maths,
        Cut_off : ((req.body.physics / 4) + (req.body.chemistry / 4) + (req.body.maths / 2)),
        
    }); 

        stu.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in saving data :' + JSON.stringify(err, undefined, 2)); }
    });
});



router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record found : ${req.params.id}`);

    Student.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    });
});

//update
router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record found : ${req.params.id}`);

    var stu = {
        name: req.body.name,
        register_number: req.body.register_number,
        physics: req.body.physics,
        chemistry: req.body.chemistry,
        maths: req.body.maths,
    };
    Student.findByIdAndUpdate(req.params.id, { $set: stu }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2)); }
    });
});


router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Student.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error while Deleting record :' + JSON.stringify(err, undefined, 2)); }
    });
});


module.exports = router;