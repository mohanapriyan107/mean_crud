const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { mongoose } = require('./database.js');
var crudapi = require('./crudapi/studentapi.js');

var app = express();

app.use(bodyParser.json());
app.use(cors());
app.listen(3000, () => console.log('Server started : 3000'));

app.use('/student', crudapi);