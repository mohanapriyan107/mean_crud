const mongoose = require('mongoose');

var Student = mongoose.model('Student', {
    name: { type: String },
    register_number: { type: Number },
    physics: { type: Number },
    chemistry: { type: Number },
    maths: { type: Number },
    Cut_off: { type: Number}
});

module.exports = { Student };